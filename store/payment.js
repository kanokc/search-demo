export const state = () => ({
  paymentModel: {
    paymentId: null,
    applicationId: null,
    policyId: null,
    date: null,
  }
})

export const mutations = {
  SET(state, data) {
    state.paymentModel = data
  }
}

export const actions = {
  setData({ commit }, data) {
    commit('SET', data)
  }
}
