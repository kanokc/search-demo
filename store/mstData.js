import tnxCollection from "./mockup/transaction";

export const state = () => {
  let lookup = {};
  tnxCollection.transaction.forEach(d => {
    lookup[d.payment_id] = d;
  });
  return {
    transaction: tnxCollection.transaction,
    transactionFind: lookup,
    transactionFocus: ""
  };
};

export const mutations = {
 
};

export const actions = {};
