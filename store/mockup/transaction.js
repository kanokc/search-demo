export default {
  transaction: [
    {
      id: 1,
      payment_id: "PA0001",
      application_id: "APP0001",
      policy_id: "PO0001",
      date: "2016-10-15 13:43:27",
      status: "Completed"
    },
    {
      id: 2,
      payment_id: "PA0002",
      application_id: "",
      policy_id: "",
      date: "2016-12-15 06:00:53",
      status: "Pending"
    },
    {
      id: 3,
      payment_id: "PA0003",
      application_id: "APP0003",
      date: "2016-04-26 06:26:28",
      policy_id: "",
      status: "Canceled"
    },
    {
      id: 4,
      payment_id: "PA0004",
      application_id: "",
      date: "2016-04-10 10:28:46",
      policy_id: "",
      status: "Pending"
    },
    {
      id: 5,
      payment_id: "PA0005",
      application_id: "APP0005",
      date: "2016-12-06 14:38:38",
      policy_id: "PO0005",
      status: "Completed"
    }
    ,
    {
      id: 6,
      payment_id: "PA0006",
      application_id: "APP0006",
      date: "2016-06-06 14:38:38",
      policy_id: "",
      status: "Canceled"
    }
    ,
    {
      id: 7,
      payment_id: "PA0007",
      application_id: "APP0007",
      date: "2016-12-07 14:38:38",
      policy_id: "PO0007",
      status: "Completed"
    }
  ]
};
